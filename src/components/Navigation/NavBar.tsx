import React from 'react';
import { Link } from 'react-router-dom';
import '../../styles/navbar.scss';

export const NavBar = () => {
  return (
    <header>
      <nav>
        <ul className='list-container'>
          <li className='list-item'>
            <Link to={'/'}> TicTacToe</Link>
          </li>
          <li className='list-item'>
            <Link to={'/timeMachine'}> Time Machine</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};
