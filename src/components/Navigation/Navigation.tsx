import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { TicTacToe } from '../../screens/TicTacToe';
import { TimeMachine } from '../../screens/TimeMachine';
import { NavBar } from './NavBar';

export enum paths {
  ticTacToe = '/',
  timeMachine = '/timeMachine',
}
export const Navigation = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path={paths.ticTacToe} element={<TicTacToe />} />
        <Route path={paths.timeMachine} element={<TimeMachine />} />
      </Routes>
    </BrowserRouter>
  );
};
