import React, { useState } from 'react';
import usePrevious from '../../hooks/usePrevious';
import { Grid } from './Grid';
import '../../styles/timeMachine.scss';

export const GameFours = () => {
  const [position, setPosition] = useState<number>(0);
  const [numbers, setNumbers] = useState<number>(0);
  const [positions, setPositions] = useState<number>(0);
  const { numbers: previous, reset } = usePrevious(numbers);

  const clickAction = (id: number) => {
    if (positions !== 0) return;
    setPosition(id);
    setNumbers(id);
  };
  const restart = () => {
    setNumbers(0);
    setPosition(0);
    setPositions(0);
    reset();
  };

  const resume = () => {
    if (positions === 0) return;
    setPositions(0);
    setPosition(previous[0]);
  };
  const goNext = () => {
    const step = positions - 1;
    if (step < 0) return;
    setPositions(step);
    setPosition(previous[step]);
  };
  const goBack = () => {
    const step = positions + 1;
    if (previous[step] === undefined) return;
    setPositions(step);
    setPosition(previous[step]);
  };

  return (
    <div className='game-time'>
      <div className='game-time-container'>
        <Grid clickAction={clickAction} position={position} />
        <div className='game-time-options'>
          <button
            onClick={goNext}
            className={'button'}
            disabled={positions - 1 < 0}
          >
            Next
          </button>
          <button
            className='button'
            disabled={previous[positions + 1] === undefined}
            onClick={goBack}
          >
            Previous
          </button>
          <button className='button' onClick={resume}>
            Resume
          </button>
          <button className='button' onClick={restart}>
            Restart
          </button>
        </div>
      </div>
    </div>
  );
};
