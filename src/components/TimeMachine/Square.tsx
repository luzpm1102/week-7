import React from 'react';

interface Props {
  id: number;
  position: number;
  clickAction: (id: number) => void;
}

export const Square = ({ id, clickAction, position }: Props) => {
  const classname =
    position === id
      ? `grid-button color-${id}`
      : `grid-button color color-${id}`;

  return (
    <>
      <button className={classname} onClick={() => clickAction(id)}></button>
    </>
  );
};
