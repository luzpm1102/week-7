import React, { useState } from 'react';
import { Square } from './Square';

interface Props {
  clickAction: (id: number) => void;
  position: number;
}

export const Grid = ({ clickAction, position }: Props) => {
  const [numbers] = useState<number[]>(
    Array.from(Array.from({ length: 16 }, (_, i) => i + 1))
  );

  return (
    <div className='grid-four-container'>
      {numbers.map((id) => (
        <Square
          key={id}
          id={id}
          clickAction={clickAction}
          position={position}
        />
      ))}
    </div>
  );
};
