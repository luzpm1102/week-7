import { useState } from 'react';
import { calculateWinner } from '../../helpers/CalculateWinner';
import { Board } from './Board';
import '../../styles/ticTacToe.scss';
interface square {
  squares: string[];
}

export const Game = () => {
  const [isWatching, setIsWatching] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [history, setHistory] = useState<square[]>([
    {
      squares: Array(9).fill(''),
    },
  ]);
  const [stepNumber, setStepNumber] = useState<number>(0);

  const [xIsNext, setXIsNext] = useState(true);
  const value = xIsNext ? 'X' : 'O';
  const current = history[stepNumber];
  const winner = current ? calculateWinner(current.squares) : 'No Yet';
  let status = '';
  if (winner) {
    status = `The Winner is `;
  } else {
    status = `Next move`;
  }
  const onClick = (i: number) => {
    const states = history.slice(0, stepNumber + 1);
    const current = states[stepNumber];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i] || isWatching) {
      return;
    }
    squares[i] = value;
    setHistory(
      states.concat([
        {
          squares: squares,
        },
      ])
    );
    setStepNumber(history.length);
    setXIsNext(!xIsNext);
  };

  const restart = () => {
    if (isPlaying) {
      return;
    }
    setStepNumber(0);
    setXIsNext(true);
    setIsWatching(false);
    setHistory([
      {
        squares: Array(9).fill(''),
      },
    ]);
  };

  const goBack = () => {
    if (isPlaying) {
      return;
    }
    const step = stepNumber - 1;

    if (step < 0) {
      return;
    }
    setXIsNext(step % 2 === 0);
    setStepNumber(step);
    setIsWatching(true);
  };
  const goNext = () => {
    if (isPlaying) {
      return;
    }
    const step = stepNumber + 1;

    if (step === history.length) {
      return;
    }
    setXIsNext(step % 2 === 0);
    setStepNumber(step);
    setIsWatching(true);
  };
  const resume = () => {
    const newLength: number = history.length - 1;
    setXIsNext(newLength % 2 === 0);
    setStepNumber(newLength);
    setIsWatching(false);
  };
  const replay = () => {
    setIsPlaying(true);
    let time = 0;
    const interval = setInterval(() => {
      if (time <= stepNumber) {
        setStepNumber(time);
        time++;
      } else {
        clearInterval(interval);
        setIsPlaying(false);
      }
    }, 500);
  };

  return (
    <div className='game'>
      <div className='game-board'>
        <Board squares={current.squares} handleClick={(i) => onClick(i)} />
      </div>
      <div className='game-options'>
        <div className='game-status'>
          <p className='game-status__p'>{status}</p>
          <div className='game-status__letter'>{winner ? winner : value}</div>
        </div>
        <button onClick={restart} className='button'>
          Restart
        </button>
        <button
          onClick={goBack}
          className='button'
          disabled={stepNumber - 1 < 0}
        >
          Previous
        </button>
        <button
          onClick={goNext}
          className='button'
          disabled={stepNumber + 2 > history.length}
        >
          Next
        </button>
        {winner || stepNumber === 9 ? (
          <button type='button' className='button' onClick={replay}>
            Replay
          </button>
        ) : (
          <button onClick={resume} className='button'>
            Resume
          </button>
        )}
      </div>
    </div>
  );
};
