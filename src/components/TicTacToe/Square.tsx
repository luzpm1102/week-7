import React from 'react';
interface Props {
  value: string;
  handleClick: () => void;
}

export const Square = ({ value, handleClick }: Props) => {
  return (
    <button className='square' onClick={handleClick}>
      {value}
    </button>
  );
};
