import { useState } from 'react';
import { Square } from './Square';

interface Props {
  squares: string[];
  handleClick: (i: number) => void;
}

export const Board = ({ squares, handleClick }: Props) => {
  return (
    <div className='grid-container'>
      {squares.map((_, index) => (
        <Square
          key={index}
          handleClick={() => handleClick(index)}
          value={squares[index]}
        />
      ))}
    </div>
  );
};
