import { useEffect, useState } from 'react';

const usePrevious = (value: number) => {
  const [numbers, setNumbers] = useState<number[]>([]);

  useEffect(() => {
    if (value === 0 || value === numbers[0]) {
      return;
    }
    setNumbers([value, ...numbers]);
  }, [value]);

  const reset = () => {
    setNumbers([]);
  };

  return { numbers, reset };
};

export default usePrevious;
