import React from 'react';
import { GameFours } from '../components/TimeMachine/GameFours';

export const TimeMachine = () => {
  return (
    <div className='container'>
      <GameFours />
    </div>
  );
};
