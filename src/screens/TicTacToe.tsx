import React from 'react';
import { Game } from '../components/TicTacToe/Game';

export const TicTacToe = () => {
  return (
    <div className='container'>
      <Game />
    </div>
  );
};
